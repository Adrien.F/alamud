# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect1
from mud.events.drug import DrugEvent

class DrugEffect(Effect1):
    EVENT = DrugEvent
